INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.', 50.0);

INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.', 150.0);

INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(1,'Vista a la piscina',1);

INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(2,'Remodelado recientemente',1);

INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(3,'Suit con jacuzzi', 2);

INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(4,'Suit con 3 habitaciones',2);
