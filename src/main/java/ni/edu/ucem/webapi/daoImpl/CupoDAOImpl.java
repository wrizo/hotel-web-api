package ni.edu.ucem.webapi.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.CupoDAO;
import ni.edu.ucem.webapi.modelo.Cupo;

@Repository
public class CupoDAOImpl implements CupoDAO
{
  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public CupoDAOImpl(final JdbcTemplate jdbcTemplate)
  {
      this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public int contar()
  {
      final String sql = "select count(*) from cupo";
      return this.jdbcTemplate.queryForObject(sql, Integer.class);
  }

  @Override
  public int contarPorCategoria(final int categoriaId)
  {
      final String sql = "select count(*) from cupo where ";
      return this.jdbcTemplate.queryForObject(sql, Integer.class);
  }

  @Override
  public Cupo obtenerPorId(final int pId)
  {
      String sql = "select * from cupo where id = ?";
      return jdbcTemplate.queryForObject(sql, new Object[]{pId},
              new BeanPropertyRowMapper<Cupo>(Cupo.class));
  }

  @Override
  public List<Cupo> obtenerTodos(final int pOffset, final int pLimit)
  {
      String sql = "select * from cupo offset ? limit ?";
      return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
              new BeanPropertyRowMapper<Cupo>(Cupo.class));
  }

  @Override
  public List<Cupo> obtenerTodosPorCategoriaId(int pCategoriaId, int pOffset, int pLimit)
  {
      final String sql = "select * from cupo where categoria = ? offset ? limit ?";
      return this.jdbcTemplate.query(sql, new Object[]{pCategoriaId, pOffset, pLimit},
              new BeanPropertyRowMapper<Cupo>(Cupo.class));
  }

}
