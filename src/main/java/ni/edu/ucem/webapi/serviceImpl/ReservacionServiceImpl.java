package ni.edu.ucem.webapi.serviceImpl;

import java.util.Date;
import java.util.Optional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.ReservacionService;

@Service
public class ReservacionServiceImpl implements ReservacionService{

    private final ReservacionDAO reservacionDAO;
    private final CuartoDAO cuartoDAO;

    @Autowired
    public ReservacionServiceImpl(final ReservacionDAO reservacionDAO,
                                  final CuartoDAO cuartoDAO){
        this.reservacionDAO = reservacionDAO;
        this.cuartoDAO = cuartoDAO;

    }

    @Transactional
    @Override
    public Reservacion obtenerPorId(Integer id){
        return this.reservacionDAO.obtenerPorId(id);
    }

    @Transactional
    @Override
    public Reservacion agregar(final Reservacion reservacion){
        Cupo cupo = this.obtenerDisponiblidadCupo(
                                    reservacion.getFechaDesde(),
                                    reservacion.getFechaHasta(),
                                    Optional.empty(),
                                    Optional.empty(),
                                    Optional.empty());

        boolean cuartoDisponible = cupo.getCuartos()
                                       .stream()
                                       .map(cuarto -> cuarto.getId())
                                       .anyMatch(cuartoId ->
                                               reservacion.getCuarto()
                                                          .getId()
                                                          .equals(cuartoId));

        if(!cuartoDisponible){
            throw new IllegalArgumentException("Disculpe, el cuarto no está disponible");
        }

        Optional<Huesped> huesped = this.reservacionDAO
                                        .obtenerHuespedPorEmail(reservacion.getHuesped()
                                                                           .getEmail());
        if(!huesped.isPresent()){
            this.reservacionDAO.agregarHuesped(reservacion.getHuesped());
            huesped = this.reservacionDAO
                          .obtenerHuespedPorEmail(reservacion.getHuesped()
                                                             .getEmail());
        }
        Cuarto cuarto = this.cuartoDAO
                            .obtenerPorId(reservacion.getCuarto().getId());

        if(cuarto == null){
            throw new IllegalArgumentException("El cuarto seleccionado no existe.");
        }

        reservacion.setHuesped(huesped.get());
        reservacion.setCuarto(cuarto);

        return this.reservacionDAO.agregar(reservacion);
    }

    @Transactional
    @Override
    public Cupo obtenerDisponiblidadCupo(final Date fechaIngreso,
                                         final Date fechaSalida,
                                         final Optional<Integer> categoriaId,
                                         final Optional<Integer> offset,
                                         final Optional<Integer> limit){

        this.validarFechas(fechaIngreso, fechaSalida);

        return this.reservacionDAO
                   .obtenerDisponiblidadCupo(
                                           fechaIngreso,
                                           fechaSalida,
                                           categoriaId,
                                           offset,
                                           limit);
    }

    private void validarFechas(final Date fechaIngreso,
                               final Date fechaSalida){
        DateTime fechaDesde = new DateTime(fechaIngreso);
        DateTime fechaHasta = new DateTime(fechaSalida);
        if(fechaDesde.isBefore(new DateTime(new Date()))){
            throw new IllegalArgumentException("La fecha de ingreso no puede ser menor a la fecha actual");
        }
        if(fechaDesde.isAfter(fechaHasta)){
            throw new IllegalArgumentException("La fecha de ingreso no puede ser mayor a la fecha salida.");
        }
    }
}
