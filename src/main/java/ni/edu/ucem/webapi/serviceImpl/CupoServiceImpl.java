package ni.edu.ucem.webapi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ni.edu.ucem.webapi.dao.CupoDAO;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.service.CupoService;

@Service
public class CupoServiceImpl implements CupoService
{
    private final CupoDAO cupoDAO;

    public CupoServiceImpl(final CupoDAO cupoDAO)
    {
        this.cupoDAO = cupoDAO;
    }

    @Override
    public Cupo obtenerCupo(final int pId)
    {
        if (pId < 0)
        {
            throw new IllegalArgumentException("ID inválido. debe ser mayor a cero.");
        }
        return this.cupoDAO.obtenerPorId(pId);
    }

    @Override
    public Pagina<Cupo> obtenerTodosCupo(Filtro paginacion)
    {
        List<Cupo> cupos;
        final int count = this.cupoDAO.contar();
        if(count > 0)
        {
            cupos = this.cupoDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            cupos = new ArrayList<Cupo>();
        }
        return new Pagina<Cupo>(cupos, count,  paginacion.getOffset(), paginacion.getLimit());
    }

    @Override
    public Pagina<Cupo> obtenerTodosCupoEnCategoria(final int pCategoriaId, final Filtro paginacion)
    {
        final int count = this.cupoDAO.contarPorCategoria(pCategoriaId);
        List<Cupo> cupos = null;
        if(count > 0)
        {
            cupos = this.cupoDAO.obtenerTodosPorCategoriaId(pCategoriaId, paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            cupos = new ArrayList<Cupo>();
        }
        return new Pagina<Cupo>(cupos, count,  paginacion.getOffset(), paginacion.getLimit());
    }
}
