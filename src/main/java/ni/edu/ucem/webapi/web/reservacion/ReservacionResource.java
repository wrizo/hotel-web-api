package ni.edu.ucem.webapi.web.reservacion;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.ReservacionService;

@RestController
@RequestMapping("/v1/reservaciones")
public class ReservacionResource {

    private final ReservacionService reservacionService;

    @Autowired
    public ReservacionResource(final ReservacionService reservacionService){
        this.reservacionService = reservacionService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtenerPorId(@PathVariable("id") final Integer id){
        final Reservacion reservacion = this.reservacionService.obtenerPorId(id);
        return new ApiResponse(Status.OK, reservacion);
    }

    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse agregarReservacion(@Valid @RequestBody final Reservacion reservacion, BindingResult result){
      if(result.hasErrors())
      {
          throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
      }

      this.reservacionService.agregar(reservacion);
      return new ApiResponse(Status.OK, reservacion);
    }
}
