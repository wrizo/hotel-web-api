package ni.edu.ucem.webapi.modelo;

import ni.edu.ucem.webapi.modelo.Cuarto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Cupo
{
    private Integer id;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="CST")
    private Date fechaIngreso;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="CST")
    private Date fechaSalida;

    private List<Cuarto> cuartos;

    public Cupo() { }

    public Cupo(final Date fechaIngreso, final Date fechaSalida, final List<Cuarto> cuartos)
    {
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.cuartos = cuartos;
    }

    public Cupo(final Integer id, final Date fechaIngreso, final Date fechaSalida, final List<Cuarto> cuartos)
    {
        this.id = id;
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.cuartos = cuartos;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getfechaSalida() {
        return fechaSalida;
    }

    public void setfechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public List<Cuarto> getCuartos() {
        return cuartos;
    }

    public void setCuartos(List<Cuarto> cuartos) {
        this.cuartos = cuartos;
    }

}
