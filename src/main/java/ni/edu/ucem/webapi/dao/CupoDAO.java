package ni.edu.ucem.webapi.dao;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import ni.edu.ucem.webapi.modelo.Cupo;

public interface CupoDAO
{
    public Cupo obtenerPorId(final int pId);

    public int contar();

    public int contarPorCategoria(final int pCategoriaId);

    public List<Cupo> obtenerTodos(final int offset, final int limit);

    public List<Cupo> obtenerTodosPorCategoriaId(final int pCategoriaId, final int offset, final int limit);

}
