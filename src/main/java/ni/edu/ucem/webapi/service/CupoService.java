package ni.edu.ucem.webapi.service;

import java.util.List;

import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;

public interface CupoService
{

    public Cupo obtenerCupo(final int pCupo);

    public Pagina<Cupo> obtenerTodosCupo(final Filtro paginacion);

    public Pagina<Cupo> obtenerTodosCupoEnCategoria(final int pCategoriaCupo, final Filtro paginacion);

}
